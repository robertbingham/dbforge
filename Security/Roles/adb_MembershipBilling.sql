﻿CREATE ROLE [adb_MembershipBilling] AUTHORIZATION [Julie.Shihadeh]
GO

EXEC sp_addrolemember N'adb_MembershipBilling', N'Accept_MembershipBillingUser_20171011'
GO

EXEC sp_addrolemember N'adb_MembershipBilling', N'Dev_MembershipBillingUser_20171011'
GO

EXEC sp_addrolemember N'adb_MembershipBilling', N'Prod_MembershipBillingUser_20171011'
GO

EXEC sp_addrolemember N'adb_MembershipBilling', N'Stage_MembershipBillingUser_20171011'
GO