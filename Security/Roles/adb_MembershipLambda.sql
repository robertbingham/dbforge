﻿CREATE ROLE [adb_MembershipLambda] AUTHORIZATION [DSI]
GO

EXEC sp_addrolemember N'adb_MembershipLambda', N'Accept_MembershipLambdaUser_20180108'
GO

EXEC sp_addrolemember N'adb_MembershipLambda', N'Dev_MembershipLambdaUser_20180108'
GO

EXEC sp_addrolemember N'adb_MembershipLambda', N'Prod_MembershipLambdaUser_20180108'
GO

EXEC sp_addrolemember N'adb_MembershipLambda', N'Stage_MembershipLambdaUser_20180108'
GO