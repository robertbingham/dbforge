﻿CREATE ROLE [udb_servicesuser] AUTHORIZATION [DSI]
GO

EXEC sp_addrolemember N'udb_servicesuser', N'Brien.Grosinsky'
GO

EXEC sp_addrolemember N'udb_servicesuser', N'Dillon.Lindstrom'
GO