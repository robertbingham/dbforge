﻿CREATE ROLE [udb_adminuser] AUTHORIZATION [Julie.Shihadeh]
GO

EXEC sp_addrolemember N'udb_adminuser', N'Adam.Oberhausen'
GO

EXEC sp_addrolemember N'udb_adminuser', N'Julie.Shihadeh'
GO

EXEC sp_addrolemember N'udb_adminuser', N'Tom.Kowalski'
GO