﻿CREATE ROLE [udb_devuser] AUTHORIZATION [Julie.Shihadeh]
GO

EXEC sp_addrolemember N'udb_devuser', N'Anton.Suarez'
GO

EXEC sp_addrolemember N'udb_devuser', N'Bilal.Hussein'
GO

EXEC sp_addrolemember N'udb_devuser', N'Dave.Lawrenchuk'
GO

EXEC sp_addrolemember N'udb_devuser', N'Eric.Muller'
GO

EXEC sp_addrolemember N'udb_devuser', N'Ethan.Evans'
GO

EXEC sp_addrolemember N'udb_devuser', N'Jared.Dobbs'
GO

EXEC sp_addrolemember N'udb_devuser', N'Jenelle.Farris'
GO

EXEC sp_addrolemember N'udb_devuser', N'Jim.Brancheau'
GO

EXEC sp_addrolemember N'udb_devuser', N'Joe.Toth'
GO

EXEC sp_addrolemember N'udb_devuser', N'Joy.Xu'
GO

EXEC sp_addrolemember N'udb_devuser', N'Kashia.Xiong'
GO

EXEC sp_addrolemember N'udb_devuser', N'Michael.Berryman'
GO

EXEC sp_addrolemember N'udb_devuser', N'Nathan.Thornton'
GO

EXEC sp_addrolemember N'udb_devuser', N'Nicole.Anderson'
GO

EXEC sp_addrolemember N'udb_devuser', N'Robert.Bingham'
GO

EXEC sp_addrolemember N'udb_devuser', N'Shereef.Marzouk'
GO

EXEC sp_addrolemember N'udb_devuser', N'Sonali.Kamra'
GO

EXEC sp_addrolemember N'udb_devuser', N'Travis.Walker'
GO

EXEC sp_addrolemember N'udb_devuser', N'Zachary.Adams'
GO