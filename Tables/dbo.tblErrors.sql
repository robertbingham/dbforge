﻿CREATE TABLE [dbo].[tblErrors] (
  [fldClientMembershipGuid] [uniqueidentifier] NULL,
  [fldDateCreated] [datetime] NOT NULL,
  [fldErrorNumber] [int] NOT NULL,
  [fldErrorMessage] [varchar](1000) NOT NULL,
  [fldErrorData] [varchar](max) NULL,
  [fldBuild] [int] NULL,
  [fldCustomerID] [int] NULL,
  [fldErrorLogID] [int] IDENTITY,
  CONSTRAINT [PK_tblErrors] PRIMARY KEY CLUSTERED ([fldErrorLogID])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

CREATE INDEX [IX_fldClientMembershipGuidfldDateCreated]
  ON [dbo].[tblErrors] ([fldClientMembershipGuid], [fldDateCreated])
  ON [PRIMARY]
GO