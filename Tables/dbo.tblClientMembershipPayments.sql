﻿CREATE TABLE [dbo].[tblClientMembershipPayments] (
  [fldClientMembershipGuid] [uniqueidentifier] NOT NULL,
  [fldDateScheduled] [date] NOT NULL,
  [fldDateApproved] [datetime] NULL,
  [fldAmount] [smallmoney] NOT NULL,
  [fldStatus] [tinyint] NOT NULL,
  [fldTransactionRef] [varchar](20) NULL,
  [fldBillingPlanScheduleID] [varchar](11) NOT NULL,
  [fldLastEdit] [datetime] NOT NULL,
  CONSTRAINT [PK_tblClientMembershipPayments_fldBillingPlanScheduleID_fldClientMembershipGuid] PRIMARY KEY CLUSTERED ([fldBillingPlanScheduleID], [fldClientMembershipGuid])
)
ON [PRIMARY]
GO

CREATE INDEX [IX_tblClientMembershipPayments_fldClientMembershipGuid]
  ON [dbo].[tblClientMembershipPayments] ([fldClientMembershipGuid])
  ON [PRIMARY]
GO

CREATE INDEX [IX_tblClientMembershipPayments_fldDateScheduled]
  ON [dbo].[tblClientMembershipPayments] ([fldDateScheduled])
  ON [PRIMARY]
GO

CREATE INDEX [IX_tblClientMembershipPayments_fldStatus]
  ON [dbo].[tblClientMembershipPayments] ([fldStatus])
  WHERE ([fldStatus] IN ((1), (3)))
  ON [PRIMARY]
GO