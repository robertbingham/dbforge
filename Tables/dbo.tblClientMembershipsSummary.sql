﻿CREATE TABLE [dbo].[tblClientMembershipsSummary] (
  [fldClientMembershipGuid] [uniqueidentifier] NOT NULL,
  [fldBillingCycle] [tinyint] NOT NULL,
  [fldBillingFrequency] [tinyint] NOT NULL,
  [fldPrice] [smallmoney] NOT NULL,
  [fldDateCreated] [datetime] NOT NULL,
  [fldCustomerID] [int] NOT NULL,
  [fldCardConnectMembershipID] [varchar](11) NOT NULL,
  [fldMerchantID] [varchar](20) NOT NULL,
  [fldLastProcessedDate] [datetime] NULL,
  [fldCardConnectAccountID] [varchar](3) NOT NULL,
  [fldCanceled] [bit] NOT NULL CONSTRAINT [df_ClientMembershipsSummary_fldCancelled_0] DEFAULT (0),
  CONSTRAINT [PK_tblClientMembershipsSummary_fldClientMembershipGuid] PRIMARY KEY CLUSTERED ([fldClientMembershipGuid])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_tblClientMembershipsSummary_fldCardConnectMembershipID]
  ON [dbo].[tblClientMembershipsSummary] ([fldCardConnectMembershipID])
  ON [PRIMARY]
GO

CREATE INDEX [IX_tblClientMembershipsSummary_fldCustomerID]
  ON [dbo].[tblClientMembershipsSummary] ([fldCustomerID])
  ON [PRIMARY]
GO